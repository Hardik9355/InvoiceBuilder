const mongoose = require("mongoose");
const CreateUser = new mongoose.Schema({
    username:{
        type: "String"
    },
    fname:{
        type:"String"
    },
    email:{
        type:"String"
    },
    password:{
        type:"String"
    }

});
const UserData = mongoose.model("UserData", CreateUser);
module.exports = UserData;