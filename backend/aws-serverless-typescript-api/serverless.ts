import type { AWS } from '@serverless/typescript';

// import UserData from "./src/functions/user";

const serverlessConfiguration: AWS = {
  org: 'hardik9355',
  app: 'dev-inzint',
  service: 'aws-serverless-typescript-api',
  frameworkVersion: '2',
  plugins: ['serverless-esbuild', 'serverless-offline', 'serverless-plugin-typescript'],
  
  provider: {
    name: 'aws',
    runtime: 'nodejs14.x',
    lambdaHashingVersion: '20201221',
    apiGateway: {
      minimumCompressionSize: 1024,
      shouldStartNameWithService: true,
    },
    environment: {
      AWS_NODEJS_CONNECTION_REUSE_ENABLED: '1',
      NODE_OPTIONS: '--enable-source-maps --stack-trace-limit=1000',
    },
   
  },
  functions: {
   user: {
      handler: 'functions.user.UserData',
      events: [
        {
          http: {
            method: 'post',
            path: '/user',
          }
        }
      ]
    }
    
    },
  
};

module.exports = serverlessConfiguration;
