import ClientData from "src/models/addclient";
const addClientData = async(event) =>{
    try{
         const{CompanyName
            ,Country
            ,fname
             ,lname
            ,email
            ,PNumber
            ,Address
            ,PostalCode
            ,City
             ,Website
             ,InvoiceCurrency
            ,Description}= JSON.parse(event.body);
        const addcompany = new ClientData({
            CompanyName
            ,Country
            ,fname
             ,lname
            ,email
            ,PNumber
            ,Address
            ,PostalCode
            ,City
             ,Website
             ,InvoiceCurrency
            ,Description
            
        });
        await addcompany.save();
        console.log("post successful");
    }
    catch(err){
        console.log(err.message);

    }

};
export default addClientData;