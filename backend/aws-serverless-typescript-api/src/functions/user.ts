import {bcrypt} from "bcrypt";
require("dotenv").config();
const env = require("dotenv");
import * as jwt from "jsonwebtoken";
import connectDB from "../config/db";
import User from "../models/user";
import {AWS} from 'aws-sdk';
env.config();
const awsConfig = {
    accessKeyId : process.env.accesskeyID ,
    secretAccessKey : process.env.secretAccessKey ,
    region: process.env.region
  }
  const SES  = new AWS.SES (awsConfig);
  connectDB();
const CreateUser = async (event) =>{
    let salt = 10;
    try{
        const {username,fname,email,password} = JSON.parse(event.body);
        bcrypt.hash (password,salt, async function(error,hash){
            if(!error){
                const createuser = new User({
                    username,
                    fname,
                    email,
                    password:hash
                });
                await createuser.save();
                console.log("Successful");
            }
        });
    }
    catch(error){
        error;
    }
};
const loginUser = async (event) => {
    let token;
    try{
      const{email,password} = JSON.parse(event.body);
      let loginuser = await  User.findOne({email:email});
      bcrypt.compare(password, loginuser.password, function (error,result){
        if(result==true){
          token = jwt.sign(
            {email:loginuser.email,  fname: loginuser.fname},
            process.env.SECRETKEY
          );
  
          
          console.log("Logged in Successfully");
          token;
          
          
        }
        else{
          console.log("Logged in unsuccessfully");
        }
      });

  }
  catch(error){
    console.log("Invalid Details");
  }
};


module.exports = CreateUser;