
import CompanyData from "src/models/addcompany";
// const mongoose = require("./config/db");
const addCompanyData = async(event) =>{
    try{
        
        const{CompanyName,TaxRegisterationNumber,fname,lname,Address,PostalCode,City,Country,PNumber,Email,Website}= JSON.parse(event.body);
        const addcompany = new CompanyData({
            CompanyName,
            TaxRegisterationNumber,
            fname,
            lname,
            Address,
            PostalCode,
            City,
            Country,
            PNumber,
            Email,
            Website
            
        });
        await addcompany.save();
        console.log("post successful");
    }
    catch(err){
        console.log(err.message);

    }

};
export default addCompanyData;
