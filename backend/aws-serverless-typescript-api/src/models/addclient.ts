import mongoose from "mongoose";
const ClientSchema = new mongoose.Schema({
    CompanyName:{
    type:"String",
        required : true
    },
    Country:{
        type:"String",
        required : true
    },
    fname:{
        type:"String",
        required:true

    },
    lname:{
        type:"String"
    },
    email:{
        type:"String"
    },
    PNumber:{
        type:"String"
    },
    Address:{
        type:"String"
    },
    PostalCode:{
        type:"String"
    },
    City:{
        type:"String"
    },
    Website:{
        type:"String"
    },
    InvoiceCurrency:{
        type:"String",
        required:true
    },
    Description:{
        type:"String"
    }

});

export default ClientSchema;
