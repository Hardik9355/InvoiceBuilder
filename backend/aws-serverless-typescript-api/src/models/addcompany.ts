import mongoose from "mongoose";
const CompanyData = new mongoose.Schema({
    CompanyName:{
        type: "String",
        required : true
    },
    TaxRegisterationNumber:{
        type:"String"
    },
    fname:{
        type:"String",
        required : true
    },
    lname:{
        type:"String"
    },
    Address: {
        type:"String"
    },
    PostalCode:{
        type:"String"
    },
    City:{
        type:"String"
    },
    Country :{
        type:"String"
    },
    PNumber:{
        type:"String",
        required : true
    },
    Email:{
        type:"String",
        required : true
    },
    Website:{
        type:"String"
    }
    
});
export default CompanyData;