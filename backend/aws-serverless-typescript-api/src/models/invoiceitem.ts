import mongoose from "mongoose";
const InvoiceItems = new mongoose.Schema({
    Name:{
        type:"String"
    },
    Hours:{
        type:"String"
    },
    PricePerHour:{
        type:"String"
    },
    Subtotal:{
        type:"String"
    },

});
export default InvoiceItems;