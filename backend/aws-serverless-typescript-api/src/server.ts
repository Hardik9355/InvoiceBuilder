import * as express from "express";
import connectDB  from "./config/db";
const app = express();
const port = 5002;
connectDB();
app.listen(port, () => {
  console.log(`Server started on http://localhost:${port}`);

});
